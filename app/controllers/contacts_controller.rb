class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :update, :destroy]

  # GET /contacts
  def index
    @contacts = Contact.all
    # Aula 19                                
    #render json: @contacts, methods: [:hello, :i18n]

    # Aula 20. Traz as duas datas: A data traduzida e a data em inglês
    #render json: @contacts, methods: :birthdate_br

    render json: @contacts
  end

  # GET /contacts/1
  def show
    # comentado na aula 20
    # render json: @contact, include: :kind

    # modificado na aula 22

    # render json: @contact.to_br

    render json: @contact, include: [:kind, :phones]
  end

  # POST /contacts
  def create
    @contact = Contact.new(contact_params)

    if @contact.save

      # comentado na aula 24

      # render json: @contact, status: :created, location: @contact

      render json: @contact, include: [:kind, :phones], status: :created, location: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /contacts/1
  def update
    if @contact.update(contact_params)
      render json: @contact, include: [:kind, :phones]
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /contacts/1
  def destroy
    @contact.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contact_params
      params.require(:contact).permit(
        :name, :email, :birthdate, :kind_id, 
        phones_attributes: [:id, :number, :_destroy]
        )
    end
end
