class Contact < ApplicationRecord
    
    # associations
    belongs_to :kind
 
    # adicionado na aula 21
    has_many :phones 

    # adicionado na aula 23
    # modificado na aula 24
    
    #accepts_nested_attributes_for :phones

    accepts_nested_attributes_for :phones, allow_destroy: true

    # adicionado na aula 22

    def as_json(options={})
        h = super(options)
        h[:birthdate] = (I18n.l(self.birthdate) unless self.birthdate.blank?)
        h 
    end


    # Comentado na aula 20

    # def birthdate_br
    #     I18n.l(self.birthdate) unless self.birthdate.blank?
    # end

    # criado na aula 20
    
    # comentado na aula 22

    # def to_br
    #     { 
    #         name: self.name, 
    #         email: self.email,
    #         birthdate: (I18n.l(self.birthdate) unless self.birthdate.blank?)
    #     }
    # end

    # def author
    #     "Jackson Pires"
    # end

    # # def as_json(option={})
    # #     super(
    # #         root:true,
    # #         methods: :author, 
    # #         include: :kind
    # #     )
    # # end

    # ##### adicionado depois

    # def kind_description
    #     self.kind.description
    # end

    # def as_json(option={})
    #     super(
    #         root:true,
    #         #methods: :author,
    #         methods: [:author, :kind_description],
    #         include: { kind: { only: :description}}
    #     )
    # end

    # def hello
    #     I18n.t('hello')
    # end

    # def i18n
    #     I18n.default_locale
    # end
end

    # aula 23

    # como saber o erro para os metodos do controller...

        # params = { contact: { 
        #     name: "jack",
        #     email: "jack2@jack.com",
        #     birthdate: "12/12/12",
        #     kind_id: 2,
        #     phones_attributes: [
        #       { number: '1234'},
        #       { number: '5678'}, 
        #       { number: '9012'} 
        #     ]
        # }}

        # x = Contact.create(params[:contact])

        # x.errors